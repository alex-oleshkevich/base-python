.ONESHELL:

SHORT_IMAGE = python
IMAGE = registry.gitlab.com/alex-oleshkevich/base-${SHORT_IMAGE}

build:
	docker build -t ${IMAGE} .
	docker tag ${IMAGE} ${IMAGE}:latest

push:
	docker push ${IMAGE}
	docker push ${IMAGE}:latest

deploy: build push
