FROM python:3.7-slim-stretch AS base

# STAGE 1: build uwsgi and install required deps
# The build artifacts will be copied to the target container on the later stage
FROM base as builder
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    postgresql-server-dev-all build-essential python3-dev musl-dev gcc && \
    apt-get -y --auto-remove purge postgresql-server-dev-all build-essential python3-dev musl-dev && \
    apt-get -y autoremove && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*
    
RUN pip install -U pip poetry --prefix=/install

# STAGE 2: copy build artifacts from the previous stage
FROM base
SHELL ["/bin/bash", "-c"]
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    tmux nano htop telnet curl gnupg wget && \
    apt-get -y autoremove && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*
ARG ENV=default
ENV ENV=$ENV

RUN adduser docker

ADD ./wait-for-it.sh /bin/wait-for-it

RUN ln -s /usr/local/bin/python3.7 /bin/python
COPY --from=builder /install /usr/local

CMD ["/usr/bin/python"]
